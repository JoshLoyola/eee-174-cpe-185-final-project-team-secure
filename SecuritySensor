/*-----------------------------------------
   CpE 185/EEE 174
   Final Group Project:Security Sensor
   Team Secure:
	Joshua Loyola, Ivan Guzman,
		Vincent Tse, Hamad Alanzi
-----------------------------------------*/

#include <stdio.h>
#include <wiringPi.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

// Timer Functions
int hour = 0, minute = 0, second = 0;

int counter(int timePassed) {
		if (minute > 59) {
			minute = minute - 60;
			++hour;
		}
		if (second > 59) {
			second = second - 60;
			++minute;
		}
		delay(timePassed);
		second += (timePassed/1000);
}

// Data Writing
int writeToText(int phase) {
	printf("Level%d %dhr %dmin %ds\n", phase, hour, minute, second);

	FILE *fp = fopen("activity.txt", "a");
	if (fp == NULL) {
		printf("Error opening txt file\n");
		return EXIT_FAILURE;
	}

	fprintf(fp ,"Level%d %dhr %dmin %ds\n", phase, hour, minute, second);

	fclose(fp);
	return 0;
}

// Email
void email() {
	char cmd[100];
	char to[] = "joshualoyola@csus.edu";
	char body[] = "activity.txt";
	char tempFile[100];

	strcpy(tempFile, tempnam("/tmp","sendmail"));

	FILE *fp = fopen(tempFile, "w");
	fprintf(fp, "%s\n", body);
	fclose(fp);

	sprintf(cmd, "sendmail %s < %s", to, tempFile);
	system(cmd);
}


// Hardware Pins
const int ledPin1 = 17;
const int ledPin2 = 27;
const int ledPin3 = 22;
const int ledPin4 = 5;
const int ledPin5 = 6;
const int ledPin6 = 13;

const int buttonPin = 18;
const int speakerPin = 23;
const int sensorPin = 24;

// State Definitions
typedef enum {
	level0,
	level1,
	level2,
	level3,
	level4,
	level5,
	level6
} systemState;

systemState phase0(void) {
	digitalWrite(ledPin1, LOW);
	digitalWrite(ledPin2, LOW);
	digitalWrite(ledPin3, LOW);
	digitalWrite(ledPin4, LOW);
	digitalWrite(ledPin5, LOW);
	digitalWrite(ledPin6, LOW);
	digitalWrite(speakerPin, LOW);
	counter(1000);
	writeToText(0);
	return level0;
}
systemState phase1(void) {
	digitalWrite(ledPin1, HIGH);
	digitalWrite(ledPin2, LOW);
	digitalWrite(ledPin3, LOW);
	digitalWrite(ledPin4, LOW);
	digitalWrite(ledPin5, LOW);
	digitalWrite(ledPin6, LOW);
	digitalWrite(speakerPin, LOW);
	counter(1000);
	writeToText(1);
	counter(9000);
	return level1;
}
systemState phase2(void) {
	digitalWrite(ledPin1, HIGH);
	digitalWrite(ledPin2, HIGH);
	digitalWrite(ledPin3, LOW);
	digitalWrite(ledPin4, LOW);
	digitalWrite(ledPin5, LOW);
	digitalWrite(ledPin6, LOW);
	digitalWrite(speakerPin, LOW);
	counter(1000);
	writeToText(2);
	counter(9000);
	return level2;
}
systemState phase3(void) {
	digitalWrite(ledPin1, HIGH);
	digitalWrite(ledPin2, HIGH);
	digitalWrite(ledPin3, HIGH);
	digitalWrite(ledPin4, LOW);
	digitalWrite(ledPin5, LOW);
	digitalWrite(ledPin6, LOW);
	digitalWrite(speakerPin, LOW);
	counter(1000);
	writeToText(3);
	counter(9000);
	return level3;
}
systemState phase4(void) {
	digitalWrite(ledPin1, HIGH);
	digitalWrite(ledPin2, HIGH);
	digitalWrite(ledPin3, HIGH);
	digitalWrite(ledPin4, HIGH);
	digitalWrite(ledPin5, LOW);
	digitalWrite(ledPin6, LOW);
	digitalWrite(speakerPin, LOW);
	counter(1000);
	writeToText(4);
	email();
	counter(9000);
	return level4;
}
systemState phase5(void) {
	digitalWrite(ledPin1, HIGH);
	digitalWrite(ledPin2, HIGH);
	digitalWrite(ledPin3, HIGH);
	digitalWrite(ledPin4, HIGH);
	digitalWrite(ledPin5, HIGH);
	digitalWrite(ledPin6, LOW);
	digitalWrite(speakerPin, LOW);
	counter(1000);
	writeToText(5);
	email();
	counter(9000);
	return level5;
}
systemState phase6(void) {
	digitalWrite(ledPin1, HIGH);
	digitalWrite(ledPin2, HIGH);
	digitalWrite(ledPin3, HIGH);
	digitalWrite(ledPin4, HIGH);
	digitalWrite(ledPin5, HIGH);
	digitalWrite(ledPin6, HIGH);
	digitalWrite(speakerPin, HIGH);
	counter(1000);
	writeToText(6);
	email();
	return level6;
} // end of States Definition

int main(void) {
	wiringPiSetupGpio();
	systemState nextState = level0;

	pinMode(ledPin1, OUTPUT);
	pinMode(ledPin2, OUTPUT);
	pinMode(ledPin3, OUTPUT);
	pinMode(ledPin4, OUTPUT);
	pinMode(ledPin5, OUTPUT);
	pinMode(ledPin6, OUTPUT);
	pinMode(speakerPin, OUTPUT);
	pinMode(buttonPin, INPUT);
	pinMode(sensorPin, INPUT);

	printf("System Start\n");
	bool scan = true;

	//Initializing an empty file to store data.
	FILE *fp = fopen("activity.txt", "w");
	if (fp == NULL) {
		printf("Error opening file\n");
		return EXIT_FAILURE;
	}
	fprintf(fp ,"Level hour minute second\n");
	fclose(fp);

	while(scan) {
		switch (nextState) {
			case level0:
				if (digitalRead(sensorPin))
					nextState = phase1();
				else if(digitalRead(buttonPin)) {
					email();
					scan = false;
				}
				else
					nextState = phase0();
				break;
			case level1:
				if (digitalRead(sensorPin))
					nextState = phase2();
				else
					nextState = phase0();
				break;
			case level2:
				if (digitalRead(sensorPin))
					nextState = phase3();
				else
					nextState = phase0();
				break;
			case level3:
				if (digitalRead(sensorPin))
					nextState = phase4();
				else
					nextState = phase0();
				break;
			case level4:
				if (digitalRead(sensorPin))
					nextState = phase5();
				else
					nextState = phase0();
				break;
			case level5:
				if (digitalRead(sensorPin))
					nextState = phase6();
				else
					nextState = phase0();
				break;
			case level6:
				if (digitalRead(buttonPin))
					nextState = phase0();
				else
					nextState = phase6();
				break;
			default:
				nextState = phase0();
				break;
		}
	}

	// Converting txt into csv
	FILE *fin = fopen("activity.txt", "r");
	if (fin = NULL) {
		printf("Error opening input file\n");
		return EXIT_FAILURE;
	}

	FILE *fout = fopen("activity.csv", "w");
	if (fout == NULL) {
		printf("Error oepning output file\n");
		return EXIT_FAILURE;
	}

	char line[1000];
	while (fgets(line, sizeof(line), fin) != NULL) {

		bool first_word = true;
		char *word = strtok(line, " \t\n");
		while(word) {
			fprintf(fout, "%s%s", first_word ? "" : ",", word);
			first_word = false;
			word = strtok(NULL, " \t\n");
		}
		fprintf(fout, "\n");
	}

	fclose(fout);
	fclose(fin);

	email();

	return 0;
}
