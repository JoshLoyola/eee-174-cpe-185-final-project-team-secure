// Ivan Guzman 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int main() {
    FILE *fin = fopen("activity.txt", "r");
    if (fin == NULL) {
        printf("Error opening input file\n");
        return EXIT_FAILURE;
    }

    FILE *fout = fopen("activity.csv", "w");
    if (fout == NULL) {
        printf("Error opening output file\n");
        return EXIT_FAILURE;
    }

    char line[1000];
    while (fgets(line, sizeof line, fin) != NULL) {
        bool first_word = true;
        char *word = strtok(line, " \t\n");
        while (word) {
            fprintf(fout, "%s%s", first_word ? "" : ",", word);
            first_word = false;
            word = strtok(NULL, " \t\n");
        }
        fprintf(fout, "\n");
    }

    fclose(fout);
    fclose(fin);

    return 0;
}