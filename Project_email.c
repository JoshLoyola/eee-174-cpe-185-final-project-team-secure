#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {

        char cmd[100];  // hold command.
        char to[] = "project_174@gmail.com"; // email sent to
        char body[] = "activity.csv";    // put csv file in body
        char tempFile[100];     // name of tempfile

        strcpy(tempFile,tempnam("/tmp","sendmail")); // generate temp file

        FILE *fp = fopen(tempFile,"w"); // open to edit email
        fprintf(fp,"%s\n",body);        // write body to email
        fclose(fp);             // close it

        sprintf(cmd,"sendmail %s < %s",to,tempFile); // prepare command
        system(cmd);     // execute it

        return 0;
}